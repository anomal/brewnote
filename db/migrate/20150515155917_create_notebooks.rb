class CreateNotebooks < ActiveRecord::Migration
  def change
    create_table :notebooks do |t|
      t.integer :user_id
      t.integer :style_id
      t.text :description

      t.timestamps
    end
  end
end
