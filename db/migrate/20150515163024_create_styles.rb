class CreateStyles < ActiveRecord::Migration
  def change
    create_table :styles do |t|
      t.text :name
      t.text :description
      t.text :abv

      t.timestamps
    end
  end
end
