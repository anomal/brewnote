class AddPrimaryFermentationIdToFermentations < ActiveRecord::Migration
  def change
    add_column :fermentations, :primary_fermentation_id, :integer
  end
end
