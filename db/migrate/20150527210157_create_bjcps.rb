class CreateBjcps < ActiveRecord::Migration
  def change
    create_table :bjcps do |t|
      t.string :code
      t.string :category
      t.string :sub_category
      t.integer :ibu_min
      t.integer :ibu_max
      t.integer :srm_min
      t.integer :srm_max
      t.decimal :og_min
      t.decimal :og_max
      t.decimal :fg_min
      t.decimal :fg_max
      t.decimal :abv_min
      t.decimal :abv_max

      t.timestamps
    end
  end
end
