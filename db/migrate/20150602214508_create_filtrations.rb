class CreateFiltrations < ActiveRecord::Migration
  def change
    create_table :filtrations do |t|
      t.integer :notebook_id
      t.integer :wort
      t.integer :blg
      t.integer :duration_hours
      t.integer :duration_minutes

      t.timestamps
    end
  end
end
