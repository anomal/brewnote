class AddTemperatureToSweetenings < ActiveRecord::Migration
  def change
    add_column :sweetenings, :temperature, :decimal
  end
end
