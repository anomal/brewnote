class AddBrewingIdToNotebooks < ActiveRecord::Migration
  def change
    add_column :notebooks, :brewing_id, :integer
  end
end
