class AddPublishedToNotebooks < ActiveRecord::Migration
  def change
    add_column :notebooks, :published, :integer
  end
end
