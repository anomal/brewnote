class CreateFormulas < ActiveRecord::Migration
  def change
    create_table :formulas do |t|
      t.integer :notebook_id
      t.text :equipment
      t.decimal :efficiency

      t.timestamps
    end
  end
end
