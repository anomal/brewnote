class CreateBottlings < ActiveRecord::Migration
  def change
    create_table :bottlings do |t|
      t.integer :fermentation_id
      t.integer :blg
      t.decimal :volume
      t.integer :days
      t.decimal :temperature
      t.string :refermentation_material
      t.decimal :refermentation_material_quantity
      t.decimal :material_per_liter

      t.timestamps
    end
  end
end
