class RemoveMeasuredFgFromBottlings < ActiveRecord::Migration
  def change
    remove_column :bottlings, :measured_fg, :decimal
  end
end
