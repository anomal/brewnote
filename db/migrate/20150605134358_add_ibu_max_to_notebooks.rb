class AddIbuMaxToNotebooks < ActiveRecord::Migration
  def change
    add_column :notebooks, :ibu_max, :integer
  end
end
