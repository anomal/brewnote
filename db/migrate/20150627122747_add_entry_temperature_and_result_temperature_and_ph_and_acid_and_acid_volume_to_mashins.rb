class AddEntryTemperatureAndResultTemperatureAndPhAndAcidAndAcidVolumeToMashins < ActiveRecord::Migration
  def change
    add_column :mashins, :ph, :decimal
    add_column :mashins, :acid, :string
    add_column :mashins, :acid_volume, :decimal
  end
end
