class CreatePrimaryFermentationDays < ActiveRecord::Migration
  def change
    create_table :primary_fermentation_days do |t|
      t.integer :primary_fermentation_id
      t.integer :day
      t.decimal :temperature

      t.timestamps
    end
  end
end
