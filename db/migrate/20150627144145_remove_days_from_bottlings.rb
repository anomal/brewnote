class RemoveDaysFromBottlings < ActiveRecord::Migration
  def change
    remove_column :bottlings, :days, :integer
  end
end
