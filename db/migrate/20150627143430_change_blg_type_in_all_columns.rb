class ChangeBlgTypeInAllColumns < ActiveRecord::Migration
  def self.up
     change_column :fermentations, :blg, :decimal
     change_column :primary_fermentations, :blg, :decimal
     change_column :secondary_fermentations, :blg, :decimal
     change_column :bottlings, :blg, :decimal
    end
 
    def self.down
     change_column :fermentations, :blg, :integer
     change_column :primary_fermentations, :blg, :integer
     change_column :secondary_fermentations, :blg, :integer
     change_column :bottlings, :blg, :integer
    end
end
