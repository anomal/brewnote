class CreateIngredients < ActiveRecord::Migration
  def change
    create_table :ingredients do |t|
      t.integer :notebook_id
      t.string :type
      t.string :name
      t.decimal :quantity

      t.timestamps
    end
  end
end
