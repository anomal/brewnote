class AddSweeteningIdToNotebooks < ActiveRecord::Migration
  def change
    add_column :notebooks, :sweetening_id, :integer
  end
end
