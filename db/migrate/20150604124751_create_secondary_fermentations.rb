class CreateSecondaryFermentations < ActiveRecord::Migration
  def change
    create_table :secondary_fermentations do |t|
      t.integer :blg
      t.decimal :volume
      t.integer :days
      t.decimal :temperature
      t.integer :fermentation_id

      t.timestamps
    end
  end
end
