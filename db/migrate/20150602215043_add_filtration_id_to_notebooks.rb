class AddFiltrationIdToNotebooks < ActiveRecord::Migration
  def change
    add_column :notebooks, :filtration_id, :integer
  end
end
