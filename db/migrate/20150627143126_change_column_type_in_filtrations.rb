class ChangeColumnTypeInFiltrations < ActiveRecord::Migration
  def self.up
     change_column :filtrations, :blg, :decimal
    end
 
    def self.down
     change_column :filtrations, :blg, :integer
    end
end
