class AddAbvToNotebooks < ActiveRecord::Migration
  def change
    add_column :notebooks, :abv, :decimal
  end
end
