class CreateMashins < ActiveRecord::Migration
  def change
    create_table :mashins do |t|
      t.integer :notebook_id
      t.decimal :pot
      t.decimal :entry_temp
      t.decimal :result_temp
      t.integer :duration_hours
      t.integer :duration_minutes

      t.timestamps
    end
  end
end
