class CreatePrimaryFermentations < ActiveRecord::Migration
  def change
    create_table :primary_fermentations do |t|
      t.integer :blg
      t.decimal :volume
      t.integer :fermentation_id

      t.timestamps
    end
  end
end
