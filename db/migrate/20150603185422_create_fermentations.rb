class CreateFermentations < ActiveRecord::Migration
  def change
    create_table :fermentations do |t|
      t.integer :notebook_id
      t.decimal :quantity
      t.integer :blg
      t.decimal :start_temperature
      t.string :yeast_form
      t.decimal :yeast_quantity
      t.text :desc

      t.timestamps
    end
  end
end
