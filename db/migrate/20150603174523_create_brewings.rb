class CreateBrewings < ActiveRecord::Migration
  def change
    create_table :brewings do |t|
      t.integer :notebook_id
      t.integer :duration_hours
      t.integer :duration_minutes
      t.text :desc

      t.timestamps
    end
  end
end
