class CreateMashinBreaks < ActiveRecord::Migration
  def change
    create_table :mashin_breaks do |t|
      t.integer :mashin_id
      t.decimal :entry_temp
      t.decimal :result_temp
      t.integer :duration_hours
      t.decimal :duration_minutes

      t.timestamps
    end
  end
end
