class AddFormulaIdToNotebooks < ActiveRecord::Migration
  def change
    add_column :notebooks, :formula_id, :integer
  end
end
