class RemoveMeasuredOgFromFermentations < ActiveRecord::Migration
  def change
    remove_column :fermentations, :measured_og, :decimal
  end
end
