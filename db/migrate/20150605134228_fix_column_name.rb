class FixColumnName < ActiveRecord::Migration
  def change
    rename_column :notebooks, :ibu, :ibu_min
  end
end
