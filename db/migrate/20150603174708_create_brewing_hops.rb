class CreateBrewingHops < ActiveRecord::Migration
  def change
    create_table :brewing_hops do |t|
      t.integer :brewing_id
      t.string :hop_name
      t.decimal :hop_quantity
      t.integer :minutes_before_end

      t.timestamps
    end
  end
end
