class CreateSweetenings < ActiveRecord::Migration
  def change
    create_table :sweetenings do |t|
      t.integer :notebook_id
      t.decimal :water
      t.decimal :ph
      t.string :acid_name
      t.decimal :acid
      t.integer :duration_hours
      t.integer :duration_minutes

      t.timestamps
    end
  end
end
