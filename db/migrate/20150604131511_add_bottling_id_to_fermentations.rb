class AddBottlingIdToFermentations < ActiveRecord::Migration
  def change
    add_column :fermentations, :bottling_id, :integer
  end
end
