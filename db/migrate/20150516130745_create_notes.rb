class CreateNotes < ActiveRecord::Migration
  def change
    create_table :notes do |t|
      t.text :name
      t.text :description
      t.datetime :date
      t.integer :notebook_id

      t.timestamps
    end
  end
end
