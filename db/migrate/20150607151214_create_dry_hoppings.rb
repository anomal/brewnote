class CreateDryHoppings < ActiveRecord::Migration
  def change
    create_table :dry_hoppings do |t|
      t.integer :secondary_fermentation_id
      t.string :name
      t.decimal :quantity
      t.integer :days_before_end

      t.timestamps
    end
  end
end
