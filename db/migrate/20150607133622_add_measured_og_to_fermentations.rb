class AddMeasuredOgToFermentations < ActiveRecord::Migration
  def change
    add_column :fermentations, :measured_og, :decimal
  end
end
