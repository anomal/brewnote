class AddSurnameAndBirthDateAndDescToUsers < ActiveRecord::Migration
  def change
    add_column :users, :surname, :string
    add_column :users, :birth_date, :date
    add_column :users, :desc, :text
  end
end
