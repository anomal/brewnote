class AddFermentationIdToNotebooks < ActiveRecord::Migration
  def change
    add_column :notebooks, :fermentation_id, :integer
  end
end
