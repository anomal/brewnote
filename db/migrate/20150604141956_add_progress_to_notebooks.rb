class AddProgressToNotebooks < ActiveRecord::Migration
  def change
    add_column :notebooks, :progress, :integer
  end
end
