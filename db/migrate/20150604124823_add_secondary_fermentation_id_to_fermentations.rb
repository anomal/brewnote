class AddSecondaryFermentationIdToFermentations < ActiveRecord::Migration
  def change
    add_column :fermentations, :secondary_fermentation_id, :integer
  end
end
