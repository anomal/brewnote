class AddMeasuredFgToBottling < ActiveRecord::Migration
  def change
    add_column :bottlings, :measured_fg, :decimal
  end
end
