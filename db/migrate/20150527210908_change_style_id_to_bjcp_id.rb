class ChangeStyleIdToBjcpId < ActiveRecord::Migration
  def change
    rename_column :notebooks, :style_id, :bjcp_id
  end
end
