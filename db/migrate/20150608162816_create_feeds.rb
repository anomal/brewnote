class CreateFeeds < ActiveRecord::Migration
  def change
    create_table :feeds do |t|
      t.integer :user_id
      t.integer :reference_id
      t.string :reference_type
      t.string :action_desc

      t.timestamps
    end
  end
end
