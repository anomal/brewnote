class AddMaltsAndHopsAndYeastAndIbuAndNumberToNotebooks < ActiveRecord::Migration
  def change
    add_column :notebooks, :malts, :string
    add_column :notebooks, :hops, :string
    add_column :notebooks, :yeast, :string
    add_column :notebooks, :ibu, :integer
    add_column :notebooks, :number, :string
  end
end
