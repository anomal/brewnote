class AddNameToFormulas < ActiveRecord::Migration
  def change
    add_column :formulas, :name, :string
  end
end
