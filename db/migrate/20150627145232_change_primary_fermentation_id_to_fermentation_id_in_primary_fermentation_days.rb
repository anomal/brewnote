class ChangePrimaryFermentationIdToFermentationIdInPrimaryFermentationDays < ActiveRecord::Migration
  def self.up
      rename_column :primary_fermentation_days, :primary_fermentation_id, :fermentation_id
    end

    def self.down
      rename_column :primary_fermentation_days, :fermentation_id, :primary_fermentation_id
    end
end
