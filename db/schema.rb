# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150611183231) do

  create_table "bjcps", force: true do |t|
    t.string   "code"
    t.string   "category"
    t.string   "sub_category"
    t.integer  "ibu_min"
    t.integer  "ibu_max"
    t.integer  "srm_min"
    t.integer  "srm_max"
    t.decimal  "og_min"
    t.decimal  "og_max"
    t.decimal  "fg_min"
    t.decimal  "fg_max"
    t.decimal  "abv_min"
    t.decimal  "abv_max"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "bottlings", force: true do |t|
    t.integer  "fermentation_id"
    t.integer  "blg"
    t.decimal  "volume"
    t.integer  "days"
    t.decimal  "temperature"
    t.string   "refermentation_material"
    t.decimal  "refermentation_material_quantity"
    t.decimal  "material_per_liter"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.decimal  "measured_fg"
  end

  create_table "brewing_hops", force: true do |t|
    t.integer  "brewing_id"
    t.string   "hop_name"
    t.decimal  "hop_quantity"
    t.integer  "minutes_before_end"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "brewings", force: true do |t|
    t.integer  "notebook_id"
    t.integer  "duration_hours"
    t.integer  "duration_minutes"
    t.text     "desc"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "dry_hoppings", force: true do |t|
    t.integer  "secondary_fermentation_id"
    t.string   "name"
    t.decimal  "quantity"
    t.integer  "days_before_end"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "feeds", force: true do |t|
    t.integer  "user_id"
    t.integer  "reference_id"
    t.string   "reference_type"
    t.string   "action_desc"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "fermentations", force: true do |t|
    t.integer  "notebook_id"
    t.decimal  "quantity"
    t.integer  "blg"
    t.decimal  "start_temperature"
    t.string   "yeast_form"
    t.decimal  "yeast_quantity"
    t.text     "desc"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "primary_fermentation_id"
    t.integer  "secondary_fermentation_id"
    t.integer  "bottling_id"
    t.decimal  "measured_og"
  end

  create_table "filtrations", force: true do |t|
    t.integer  "notebook_id"
    t.integer  "wort"
    t.integer  "blg"
    t.integer  "duration_hours"
    t.integer  "duration_minutes"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "desc"
  end

  create_table "follows", force: true do |t|
    t.string   "follower_type"
    t.integer  "follower_id"
    t.string   "followable_type"
    t.integer  "followable_id"
    t.datetime "created_at"
  end

  add_index "follows", ["followable_id", "followable_type"], name: "fk_followables"
  add_index "follows", ["follower_id", "follower_type"], name: "fk_follows"

  create_table "formulas", force: true do |t|
    t.integer  "notebook_id"
    t.text     "equipment"
    t.decimal  "efficiency"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
  end

  create_table "ingredients", force: true do |t|
    t.integer  "notebook_id"
    t.string   "kind"
    t.string   "name"
    t.decimal  "quantity"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "likes", force: true do |t|
    t.string   "liker_type"
    t.integer  "liker_id"
    t.string   "likeable_type"
    t.integer  "likeable_id"
    t.datetime "created_at"
  end

  add_index "likes", ["likeable_id", "likeable_type"], name: "fk_likeables"
  add_index "likes", ["liker_id", "liker_type"], name: "fk_likes"

  create_table "mashin_breaks", force: true do |t|
    t.integer  "mashin_id"
    t.decimal  "entry_temp"
    t.decimal  "result_temp"
    t.integer  "duration_hours"
    t.decimal  "duration_minutes"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "mashins", force: true do |t|
    t.integer  "notebook_id"
    t.decimal  "pot"
    t.decimal  "entry_temp"
    t.decimal  "result_temp"
    t.integer  "duration_hours"
    t.integer  "duration_minutes"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "desc"
  end

  create_table "mentions", force: true do |t|
    t.string   "mentioner_type"
    t.integer  "mentioner_id"
    t.string   "mentionable_type"
    t.integer  "mentionable_id"
    t.datetime "created_at"
  end

  add_index "mentions", ["mentionable_id", "mentionable_type"], name: "fk_mentionables"
  add_index "mentions", ["mentioner_id", "mentioner_type"], name: "fk_mentions"

  create_table "notebooks", force: true do |t|
    t.integer  "user_id"
    t.integer  "bjcp_id"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.string   "malts"
    t.string   "hops"
    t.string   "yeast"
    t.integer  "ibu_min"
    t.string   "number"
    t.integer  "mashin_id"
    t.integer  "filtration_id"
    t.integer  "sweetening_id"
    t.integer  "brewing_id"
    t.integer  "fermentation_id"
    t.integer  "progress"
    t.integer  "ibu_max"
    t.decimal  "abv"
    t.integer  "published"
    t.integer  "formula_id"
  end

  create_table "notes", force: true do |t|
    t.text     "name"
    t.text     "description"
    t.datetime "date"
    t.integer  "notebook_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "primary_fermentation_days", force: true do |t|
    t.integer  "primary_fermentation_id"
    t.integer  "day"
    t.decimal  "temperature"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "primary_fermentations", force: true do |t|
    t.integer  "blg"
    t.decimal  "volume"
    t.integer  "fermentation_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "secondary_fermentations", force: true do |t|
    t.integer  "blg"
    t.decimal  "volume"
    t.integer  "days"
    t.decimal  "temperature"
    t.integer  "fermentation_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "styles", force: true do |t|
    t.text     "name"
    t.text     "description"
    t.text     "abv"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sweetenings", force: true do |t|
    t.integer  "notebook_id"
    t.decimal  "water"
    t.decimal  "ph"
    t.string   "acid_name"
    t.decimal  "acid"
    t.integer  "duration_hours"
    t.integer  "duration_minutes"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "desc"
    t.decimal  "temperature"
  end

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "surname"
    t.date     "birth_date"
    t.text     "desc"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

end
