require 'test_helper'

class BottlingsControllerTest < ActionController::TestCase
  setup do
    @bottling = bottlings(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:bottlings)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create bottling" do
    assert_difference('Bottling.count') do
      post :create, bottling: { blg: @bottling.blg, days: @bottling.days, fermentation_id: @bottling.fermentation_id, material_per_liter: @bottling.material_per_liter, refermentation_material: @bottling.refermentation_material, refermentation_material_quantity: @bottling.refermentation_material_quantity, temperature: @bottling.temperature, volume: @bottling.volume }
    end

    assert_redirected_to bottling_path(assigns(:bottling))
  end

  test "should show bottling" do
    get :show, id: @bottling
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @bottling
    assert_response :success
  end

  test "should update bottling" do
    patch :update, id: @bottling, bottling: { blg: @bottling.blg, days: @bottling.days, fermentation_id: @bottling.fermentation_id, material_per_liter: @bottling.material_per_liter, refermentation_material: @bottling.refermentation_material, refermentation_material_quantity: @bottling.refermentation_material_quantity, temperature: @bottling.temperature, volume: @bottling.volume }
    assert_redirected_to bottling_path(assigns(:bottling))
  end

  test "should destroy bottling" do
    assert_difference('Bottling.count', -1) do
      delete :destroy, id: @bottling
    end

    assert_redirected_to bottlings_path
  end
end
