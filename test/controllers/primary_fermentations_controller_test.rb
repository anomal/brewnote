require 'test_helper'

class PrimaryFermentationsControllerTest < ActionController::TestCase
  setup do
    @primary_fermentation = primary_fermentations(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:primary_fermentations)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create primary_fermentation" do
    assert_difference('PrimaryFermentation.count') do
      post :create, primary_fermentation: { blg: @primary_fermentation.blg, fermentation_id: @primary_fermentation.fermentation_id, volume: @primary_fermentation.volume }
    end

    assert_redirected_to primary_fermentation_path(assigns(:primary_fermentation))
  end

  test "should show primary_fermentation" do
    get :show, id: @primary_fermentation
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @primary_fermentation
    assert_response :success
  end

  test "should update primary_fermentation" do
    patch :update, id: @primary_fermentation, primary_fermentation: { blg: @primary_fermentation.blg, fermentation_id: @primary_fermentation.fermentation_id, volume: @primary_fermentation.volume }
    assert_redirected_to primary_fermentation_path(assigns(:primary_fermentation))
  end

  test "should destroy primary_fermentation" do
    assert_difference('PrimaryFermentation.count', -1) do
      delete :destroy, id: @primary_fermentation
    end

    assert_redirected_to primary_fermentations_path
  end
end
