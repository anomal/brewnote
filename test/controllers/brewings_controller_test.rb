require 'test_helper'

class BrewingsControllerTest < ActionController::TestCase
  setup do
    @brewing = brewings(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:brewings)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create brewing" do
    assert_difference('Brewing.count') do
      post :create, brewing: { desc: @brewing.desc, duration_hours: @brewing.duration_hours, duration_minutes: @brewing.duration_minutes, notebook_id: @brewing.notebook_id }
    end

    assert_redirected_to brewing_path(assigns(:brewing))
  end

  test "should show brewing" do
    get :show, id: @brewing
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @brewing
    assert_response :success
  end

  test "should update brewing" do
    patch :update, id: @brewing, brewing: { desc: @brewing.desc, duration_hours: @brewing.duration_hours, duration_minutes: @brewing.duration_minutes, notebook_id: @brewing.notebook_id }
    assert_redirected_to brewing_path(assigns(:brewing))
  end

  test "should destroy brewing" do
    assert_difference('Brewing.count', -1) do
      delete :destroy, id: @brewing
    end

    assert_redirected_to brewings_path
  end
end
