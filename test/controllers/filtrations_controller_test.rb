require 'test_helper'

class FiltrationsControllerTest < ActionController::TestCase
  setup do
    @filtration = filtrations(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:filtrations)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create filtration" do
    assert_difference('Filtration.count') do
      post :create, filtration: { blg: @filtration.blg, duration_hours: @filtration.duration_hours, duration_minutes: @filtration.duration_minutes, notebook_id: @filtration.notebook_id, wort: @filtration.wort }
    end

    assert_redirected_to filtration_path(assigns(:filtration))
  end

  test "should show filtration" do
    get :show, id: @filtration
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @filtration
    assert_response :success
  end

  test "should update filtration" do
    patch :update, id: @filtration, filtration: { blg: @filtration.blg, duration_hours: @filtration.duration_hours, duration_minutes: @filtration.duration_minutes, notebook_id: @filtration.notebook_id, wort: @filtration.wort }
    assert_redirected_to filtration_path(assigns(:filtration))
  end

  test "should destroy filtration" do
    assert_difference('Filtration.count', -1) do
      delete :destroy, id: @filtration
    end

    assert_redirected_to filtrations_path
  end
end
