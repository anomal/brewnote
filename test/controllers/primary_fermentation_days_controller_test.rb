require 'test_helper'

class PrimaryFermentationDaysControllerTest < ActionController::TestCase
  setup do
    @primary_fermentation_day = primary_fermentation_days(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:primary_fermentation_days)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create primary_fermentation_day" do
    assert_difference('PrimaryFermentationDay.count') do
      post :create, primary_fermentation_day: { day: @primary_fermentation_day.day, primary_fermentation_id: @primary_fermentation_day.primary_fermentation_id, temperature: @primary_fermentation_day.temperature }
    end

    assert_redirected_to primary_fermentation_day_path(assigns(:primary_fermentation_day))
  end

  test "should show primary_fermentation_day" do
    get :show, id: @primary_fermentation_day
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @primary_fermentation_day
    assert_response :success
  end

  test "should update primary_fermentation_day" do
    patch :update, id: @primary_fermentation_day, primary_fermentation_day: { day: @primary_fermentation_day.day, primary_fermentation_id: @primary_fermentation_day.primary_fermentation_id, temperature: @primary_fermentation_day.temperature }
    assert_redirected_to primary_fermentation_day_path(assigns(:primary_fermentation_day))
  end

  test "should destroy primary_fermentation_day" do
    assert_difference('PrimaryFermentationDay.count', -1) do
      delete :destroy, id: @primary_fermentation_day
    end

    assert_redirected_to primary_fermentation_days_path
  end
end
