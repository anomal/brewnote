require 'test_helper'

class BrewingHopsControllerTest < ActionController::TestCase
  setup do
    @brewing_hop = brewing_hops(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:brewing_hops)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create brewing_hop" do
    assert_difference('BrewingHop.count') do
      post :create, brewing_hop: { brewing_id: @brewing_hop.brewing_id, hop_name: @brewing_hop.hop_name, hop_quantity: @brewing_hop.hop_quantity, minutes_before_end: @brewing_hop.minutes_before_end }
    end

    assert_redirected_to brewing_hop_path(assigns(:brewing_hop))
  end

  test "should show brewing_hop" do
    get :show, id: @brewing_hop
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @brewing_hop
    assert_response :success
  end

  test "should update brewing_hop" do
    patch :update, id: @brewing_hop, brewing_hop: { brewing_id: @brewing_hop.brewing_id, hop_name: @brewing_hop.hop_name, hop_quantity: @brewing_hop.hop_quantity, minutes_before_end: @brewing_hop.minutes_before_end }
    assert_redirected_to brewing_hop_path(assigns(:brewing_hop))
  end

  test "should destroy brewing_hop" do
    assert_difference('BrewingHop.count', -1) do
      delete :destroy, id: @brewing_hop
    end

    assert_redirected_to brewing_hops_path
  end
end
