require 'test_helper'

class DryHoppingsControllerTest < ActionController::TestCase
  setup do
    @dry_hopping = dry_hoppings(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:dry_hoppings)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create dry_hopping" do
    assert_difference('DryHopping.count') do
      post :create, dry_hopping: { days_before_end: @dry_hopping.days_before_end, name: @dry_hopping.name, quantity: @dry_hopping.quantity, secondary_fermentation_id: @dry_hopping.secondary_fermentation_id }
    end

    assert_redirected_to dry_hopping_path(assigns(:dry_hopping))
  end

  test "should show dry_hopping" do
    get :show, id: @dry_hopping
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @dry_hopping
    assert_response :success
  end

  test "should update dry_hopping" do
    patch :update, id: @dry_hopping, dry_hopping: { days_before_end: @dry_hopping.days_before_end, name: @dry_hopping.name, quantity: @dry_hopping.quantity, secondary_fermentation_id: @dry_hopping.secondary_fermentation_id }
    assert_redirected_to dry_hopping_path(assigns(:dry_hopping))
  end

  test "should destroy dry_hopping" do
    assert_difference('DryHopping.count', -1) do
      delete :destroy, id: @dry_hopping
    end

    assert_redirected_to dry_hoppings_path
  end
end
