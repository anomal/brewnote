require 'test_helper'

class MashinBreaksControllerTest < ActionController::TestCase
  setup do
    @mashin_break = mashin_breaks(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:mashin_breaks)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create mashin_break" do
    assert_difference('MashinBreak.count') do
      post :create, mashin_break: { duration_hours: @mashin_break.duration_hours, duration_minutes: @mashin_break.duration_minutes, entry_temp: @mashin_break.entry_temp, mashin_id: @mashin_break.mashin_id, result_temp: @mashin_break.result_temp }
    end

    assert_redirected_to mashin_break_path(assigns(:mashin_break))
  end

  test "should show mashin_break" do
    get :show, id: @mashin_break
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @mashin_break
    assert_response :success
  end

  test "should update mashin_break" do
    patch :update, id: @mashin_break, mashin_break: { duration_hours: @mashin_break.duration_hours, duration_minutes: @mashin_break.duration_minutes, entry_temp: @mashin_break.entry_temp, mashin_id: @mashin_break.mashin_id, result_temp: @mashin_break.result_temp }
    assert_redirected_to mashin_break_path(assigns(:mashin_break))
  end

  test "should destroy mashin_break" do
    assert_difference('MashinBreak.count', -1) do
      delete :destroy, id: @mashin_break
    end

    assert_redirected_to mashin_breaks_path
  end
end
