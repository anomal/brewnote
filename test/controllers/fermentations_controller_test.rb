require 'test_helper'

class FermentationsControllerTest < ActionController::TestCase
  setup do
    @fermentation = fermentations(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:fermentations)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create fermentation" do
    assert_difference('Fermentation.count') do
      post :create, fermentation: { blg: @fermentation.blg, desc: @fermentation.desc, notebook_id: @fermentation.notebook_id, quantity: @fermentation.quantity, start_temperature: @fermentation.start_temperature, yeast_form: @fermentation.yeast_form, yeast_quantity: @fermentation.yeast_quantity }
    end

    assert_redirected_to fermentation_path(assigns(:fermentation))
  end

  test "should show fermentation" do
    get :show, id: @fermentation
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @fermentation
    assert_response :success
  end

  test "should update fermentation" do
    patch :update, id: @fermentation, fermentation: { blg: @fermentation.blg, desc: @fermentation.desc, notebook_id: @fermentation.notebook_id, quantity: @fermentation.quantity, start_temperature: @fermentation.start_temperature, yeast_form: @fermentation.yeast_form, yeast_quantity: @fermentation.yeast_quantity }
    assert_redirected_to fermentation_path(assigns(:fermentation))
  end

  test "should destroy fermentation" do
    assert_difference('Fermentation.count', -1) do
      delete :destroy, id: @fermentation
    end

    assert_redirected_to fermentations_path
  end
end
