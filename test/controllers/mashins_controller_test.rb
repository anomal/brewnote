require 'test_helper'

class MashinsControllerTest < ActionController::TestCase
  setup do
    @mashin = mashins(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:mashins)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create mashin" do
    assert_difference('Mashin.count') do
      post :create, mashin: { duration_hours: @mashin.duration_hours, duration_minutes: @mashin.duration_minutes, entry_temp: @mashin.entry_temp, notebook_id: @mashin.notebook_id, pot: @mashin.pot, result_temp: @mashin.result_temp }
    end

    assert_redirected_to mashin_path(assigns(:mashin))
  end

  test "should show mashin" do
    get :show, id: @mashin
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @mashin
    assert_response :success
  end

  test "should update mashin" do
    patch :update, id: @mashin, mashin: { duration_hours: @mashin.duration_hours, duration_minutes: @mashin.duration_minutes, entry_temp: @mashin.entry_temp, notebook_id: @mashin.notebook_id, pot: @mashin.pot, result_temp: @mashin.result_temp }
    assert_redirected_to mashin_path(assigns(:mashin))
  end

  test "should destroy mashin" do
    assert_difference('Mashin.count', -1) do
      delete :destroy, id: @mashin
    end

    assert_redirected_to mashins_path
  end
end
