require 'test_helper'

class SweeteningsControllerTest < ActionController::TestCase
  setup do
    @sweetening = sweetenings(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:sweetenings)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create sweetening" do
    assert_difference('Sweetening.count') do
      post :create, sweetening: { acid: @sweetening.acid, acid_name: @sweetening.acid_name, duration_hours: @sweetening.duration_hours, duration_minutes: @sweetening.duration_minutes, notebook_id: @sweetening.notebook_id, ph: @sweetening.ph, water: @sweetening.water }
    end

    assert_redirected_to sweetening_path(assigns(:sweetening))
  end

  test "should show sweetening" do
    get :show, id: @sweetening
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @sweetening
    assert_response :success
  end

  test "should update sweetening" do
    patch :update, id: @sweetening, sweetening: { acid: @sweetening.acid, acid_name: @sweetening.acid_name, duration_hours: @sweetening.duration_hours, duration_minutes: @sweetening.duration_minutes, notebook_id: @sweetening.notebook_id, ph: @sweetening.ph, water: @sweetening.water }
    assert_redirected_to sweetening_path(assigns(:sweetening))
  end

  test "should destroy sweetening" do
    assert_difference('Sweetening.count', -1) do
      delete :destroy, id: @sweetening
    end

    assert_redirected_to sweetenings_path
  end
end
