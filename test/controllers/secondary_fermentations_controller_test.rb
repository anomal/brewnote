require 'test_helper'

class SecondaryFermentationsControllerTest < ActionController::TestCase
  setup do
    @secondary_fermentation = secondary_fermentations(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:secondary_fermentations)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create secondary_fermentation" do
    assert_difference('SecondaryFermentation.count') do
      post :create, secondary_fermentation: { blg: @secondary_fermentation.blg, days: @secondary_fermentation.days, fermentation_id: @secondary_fermentation.fermentation_id, temperature: @secondary_fermentation.temperature, volume: @secondary_fermentation.volume }
    end

    assert_redirected_to secondary_fermentation_path(assigns(:secondary_fermentation))
  end

  test "should show secondary_fermentation" do
    get :show, id: @secondary_fermentation
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @secondary_fermentation
    assert_response :success
  end

  test "should update secondary_fermentation" do
    patch :update, id: @secondary_fermentation, secondary_fermentation: { blg: @secondary_fermentation.blg, days: @secondary_fermentation.days, fermentation_id: @secondary_fermentation.fermentation_id, temperature: @secondary_fermentation.temperature, volume: @secondary_fermentation.volume }
    assert_redirected_to secondary_fermentation_path(assigns(:secondary_fermentation))
  end

  test "should destroy secondary_fermentation" do
    assert_difference('SecondaryFermentation.count', -1) do
      delete :destroy, id: @secondary_fermentation
    end

    assert_redirected_to secondary_fermentations_path
  end
end
