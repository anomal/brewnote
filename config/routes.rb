Rails.application.routes.draw do
  
  resources :formulas

  resources :dry_hoppings

  resources :ingredients

  resources :bottlings

  resources :secondary_fermentations

  resources :primary_fermentation_days

  resources :primary_fermentations

  resources :fermentations

  resources :brewing_hops

  resources :brewings

  resources :sweetenings

  resources :filtrations

  resources :mashin_breaks

  resources :mashins

  resources :notes

  resources :styles

  resources :notebooks

  #resources :users
  
  get 'users' => "users#index", :as => :users_index

  get 'users/show/:id' => "users#show", :as => :user
  
  get 'users/follow/:id' => "users#follow", :as => :user_follow
  
  get 'users/unfollow/:id' => "users#unfollow", :as => :user_unfollow
  
  #get 'users/sign_up'

  get 'home/index'
  
  get "/ingredients/new/:id" => "ingredients#new", :as => :new_ingredient_with_param
   
  get "/notes/new/:id" => "notes#new", :as => :new_note_with_param
  
  get "/mashins/new/:id" => "mashins#new", :as => :new_mashin_with_param
   
  get "/mashin_breaks/new/:id" => "mashin_breaks#new", :as => :new_mashin_break_with_param
  
  get "/filtrations/new/:id" => "filtrations#new", :as => :new_filtration_with_param
  
  get "/sweetenings/new/:id" => "sweetenings#new", :as => :new_sweetening_with_param
  
  get "/brewings/new/:id" => "brewings#new", :as => :new_brewing_with_param
  
  get "/brewing_hops/new/:id" => "brewing_hops#new", :as => :new_brewing_hop_with_param
  
  get "/fermentations/new/:id" => "fermentations#new", :as => :new_fermentation_with_param
  
  get "/primary_fermentations/new/:id" => "primary_fermentations#new", :as => :new_primary_fermentation_with_param
  
  get "/primary_fermentation_days/new/:id" => "primary_fermentation_days#new", :as => :new_primary_fermentation_day_with_param
  
  get "/secondary_fermentations/new/:id" => "secondary_fermentations#new", :as => :new_secondary_fermentation_with_param
  
  get "/dry_hoppings/new/:id" => "dry_hoppings#new", :as => :new_dry_hopping_with_param
  
  get "/bottlings/new/:id" => "bottlings#new", :as => :new_bottling_with_param
  
  get "/formulas/new/:id" => "formulas#new", :as => :new_formula_with_param
  
  get "/formulas/like/:id" => "formulas#like", :as => :formula_like
  
  get "/formulas/unlike/:id" => "formulas#unlike", :as => :formula_unlike
  
  get "/about" => "home#about", :as => :about
  
  
    
  #devise_for :users
  devise_for :users do
    get '/users/sign_out' => 'devise/sessions#destroy'
  end
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
  
  root to: "feeds#index"
  
end
