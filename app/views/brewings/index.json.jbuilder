json.array!(@brewings) do |brewing|
  json.extract! brewing, :id, :notebook_id, :duration_hours, :duration_minutes, :desc
  json.url brewing_url(brewing, format: :json)
end
