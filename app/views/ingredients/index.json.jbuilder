json.array!(@ingredients) do |ingredient|
  json.extract! ingredient, :id, :notebook_id, :type, :name, :quantity
  json.url ingredient_url(ingredient, format: :json)
end
