json.array!(@bottlings) do |bottling|
  json.extract! bottling, :id, :fermentation_id, :blg, :volume, :days, :temperature, :refermentation_material, :refermentation_material_quantity, :material_per_liter
  json.url bottling_url(bottling, format: :json)
end
