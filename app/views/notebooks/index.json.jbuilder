json.array!(@notebooks) do |notebook|
  json.extract! notebook, :id, :user_id, :style_id, :description
  json.url notebook_url(notebook, format: :json)
end
