json.array!(@brewing_hops) do |brewing_hop|
  json.extract! brewing_hop, :id, :brewing_id, :hop_name, :hop_quantity, :minutes_before_end
  json.url brewing_hop_url(brewing_hop, format: :json)
end
