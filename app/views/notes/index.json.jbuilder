json.array!(@notes) do |note|
  json.extract! note, :id, :name, :description, :date, :notebook_id
  json.url note_url(note, format: :json)
end
