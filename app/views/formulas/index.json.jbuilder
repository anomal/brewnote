json.array!(@formulas) do |formula|
  json.extract! formula, :id, :notebook_id, :equipment, :efficiency
  json.url formula_url(formula, format: :json)
end
