json.array!(@secondary_fermentations) do |secondary_fermentation|
  json.extract! secondary_fermentation, :id, :blg, :volume, :days, :temperature, :fermentation_id
  json.url secondary_fermentation_url(secondary_fermentation, format: :json)
end
