json.array!(@primary_fermentations) do |primary_fermentation|
  json.extract! primary_fermentation, :id, :blg, :volume, :fermentation_id
  json.url primary_fermentation_url(primary_fermentation, format: :json)
end
