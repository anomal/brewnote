json.array!(@primary_fermentation_days) do |primary_fermentation_day|
  json.extract! primary_fermentation_day, :id, :primary_fermentation_id, :day, :temperature
  json.url primary_fermentation_day_url(primary_fermentation_day, format: :json)
end
