json.array!(@mashins) do |mashin|
  json.extract! mashin, :id, :notebook_id, :pot, :entry_temp, :result_temp, :duration_hours, :duration_minutes
  json.url mashin_url(mashin, format: :json)
end
