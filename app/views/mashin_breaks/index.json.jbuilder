json.array!(@mashin_breaks) do |mashin_break|
  json.extract! mashin_break, :id, :mashin_id, :entry_temp, :result_temp, :duration_hours, :duration_minutes
  json.url mashin_break_url(mashin_break, format: :json)
end
