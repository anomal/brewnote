json.array!(@dry_hoppings) do |dry_hopping|
  json.extract! dry_hopping, :id, :secondary_fermentation_id, :name, :quantity, :days_before_end
  json.url dry_hopping_url(dry_hopping, format: :json)
end
