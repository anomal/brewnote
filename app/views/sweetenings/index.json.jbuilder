json.array!(@sweetenings) do |sweetening|
  json.extract! sweetening, :id, :notebook_id, :water, :ph, :acid_name, :acid, :duration_hours, :duration_minutes
  json.url sweetening_url(sweetening, format: :json)
end
