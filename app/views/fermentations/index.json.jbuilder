json.array!(@fermentations) do |fermentation|
  json.extract! fermentation, :id, :notebook_id, :quantity, :blg, :start_temperature, :yeast_form, :yeast_quantity, :desc
  json.url fermentation_url(fermentation, format: :json)
end
