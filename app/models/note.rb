class Note < ActiveRecord::Base
  belongs_to :notebook
  
  validates :name, presence: true
  validates :notebook_id, presence: true, numericality: true
  validates :description, presence: true
end
