class Mashin < ActiveRecord::Base
  belongs_to :notebook
  has_many :mashin_breaks, :dependent => :destroy
  
  validates :pot, presence: true, numericality: true
  validates :entry_temp, presence: true, numericality: true
end
