class PrimaryFermentationDay < ActiveRecord::Base
  belongs_to :fermentation
  
  validates :day, presence: true, numericality: true
  validates :temperature, presence: true, numericality: true
end
