class Ingredient < ActiveRecord::Base
  belongs_to :notebook
  
  validates :kind, presence: true
  validates :name, presence: true
  validates :quantity, presence: true, numericality: true
  
end
