class Bjcp < ActiveRecord::Base
  has_many :notebooks
  
  def full_name
      "#{code} #{sub_category}"
  end
end
