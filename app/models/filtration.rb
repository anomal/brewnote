class Filtration < ActiveRecord::Base
   belongs_to :notebook
   
   validates :wort, presence: true, numericality: true
   validates :blg, presence: true, numericality: true
   validates :duration_hours, presence: true, numericality: true
   validates :duration_minutes, presence: true, numericality: {only_integer: true, greater_than_or_equal_to: 0, less_than_or_equal_to: 59}
end
