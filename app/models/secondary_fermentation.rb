class SecondaryFermentation < ActiveRecord::Base
  belongs_to :fermentation
  has_many :dry_hoppings
  
  validates :days, presence: true, numericality: true
  validates :temperature, presence: true, numericality: true
  validates :volume , presence: true, numericality: true
  validates :blg, presence: true, numericality: true
end
