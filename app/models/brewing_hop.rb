class BrewingHop < ActiveRecord::Base
  belongs_to :brewing
  #before_validation :brewing_length
  
  validates :hop_name, presence: true
  validates :hop_quantity, presence: true, numericality: true
  validates :minutes_before_end, presence: true, numericality: {only_integer: true, greater_than_or_equal_to: 0}
end

