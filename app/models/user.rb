class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
         
  
  validates :name, presence: true
  validates :surname, presence: true
  validates :birth_date, presence: true
  #attr_accessible :name
  
  has_many :notebooks
  has_many :feeds, :dependent => :destroy
  
  acts_as_follower
  acts_as_followable
  acts_as_mentionable
  acts_as_liker
  
  has_attached_file :avatar, :styles => { :medium => "300x300>", :thumb => "160x160>" }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/
  
  def full_name
    "#{name} #{surname}"
  end
end
