class Bottling < ActiveRecord::Base
  belongs_to :fermentation
  before_save :set_material_per_liter
  
  validates :temperature, presence: true, numericality: true
  validates :volume , presence: true, numericality: true
  validates :blg, presence: true, numericality: true
  validates :refermentation_material_quantity, presence: true, numericality: true
  
  private
  def set_material_per_liter
    unless self.refermentation_material_quantity.nil? && self.volume.nil?
      self.material_per_liter = self.refermentation_material_quantity / self.volume
    end
  end
end
