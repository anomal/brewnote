class Sweetening < ActiveRecord::Base
  belongs_to :notebook
  
  validates :water, presence: true, numericality: true
  validates :temperature, presence: true, numericality: true
  validates :ph, presence: true, numericality: true
  validates :acid_name, presence: true
  validates :acid, presence: true, numericality: true
  validates :duration_hours, presence: true, numericality: true
  validates :duration_minutes, presence: true, numericality: {only_integer: true, greater_than_or_equal_to: 0, less_than_or_equal_to: 59}
end
