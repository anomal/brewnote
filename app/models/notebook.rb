class Notebook < ActiveRecord::Base
  belongs_to :user
  belongs_to :bjcp
  has_many :notes, :dependent => :destroy
  has_many :ingredients, :dependent => :destroy
  has_one :mashin, :dependent => :destroy
  has_one :filtration, :dependent => :destroy
  has_one :sweetening, :dependent => :destroy
  has_one :brewing, :dependent => :destroy
  has_one :fermentation, :dependent => :destroy
  has_one :formula, :dependent => :destroy
  
  validates :name, presence: true
  validates :bjcp_id, presence: true, numericality: true
  validates :number, presence: true
  
  before_save :add_progress
  before_save :set_ibu
  before_save :set_published
  
  private
    def add_progress
      if self.progress.nil?
        self.progress = 0
      end
    end
    
    def set_published
      if self.published.nil?
        self.published = 0
      end
    end
    
    def set_ibu
      unless self.bjcp_id.nil?
        @bjcp = self.bjcp
        self.ibu_min = @bjcp.ibu_min
        self.ibu_max = @bjcp.ibu_max
      else
        self.ibu_min = "n/a"
        self.ibu_max = "n/a"
      end
    end
end
