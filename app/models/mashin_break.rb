class MashinBreak < ActiveRecord::Base
  belongs_to :mashin
  
  validates :entry_temp, presence: true, numericality: true
  validates :result_temp, presence: true, numericality: true
  validates :duration_hours, presence: true, numericality: true
  validates :duration_minutes, presence: true, numericality: {only_integer: true, greater_than_or_equal_to: 0, less_than_or_equal_to: 59}
  validates :step_type, presence: true
end
