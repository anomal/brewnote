class DryHopping < ActiveRecord::Base
  belongs_to :secondary_fermentation
  
  validates :name, presence: true
  validates :quantity, presence: true, numericality: true
  validates :days_before_end, presence: true, numericality: true
end
