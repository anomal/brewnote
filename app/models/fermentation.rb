class Fermentation < ActiveRecord::Base
  belongs_to :notebook
  #has_one :primary_fermentation, :dependent => :destroy
  has_many :primary_fermentation_days, :dependent => :destroy
  has_one :secondary_fermentation, :dependent => :destroy
  has_one :bottling, :dependent => :destroy
  
  validates :quantity, presence: true, numericality: true
  validates :blg, presence: true, numericality: true
  validates :start_temperature, presence: true, numericality: true
  validates :yeast_quantity, presence: true, numericality: true
end
