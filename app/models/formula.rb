class Formula < ActiveRecord::Base
  belongs_to :notebook
  acts_as_likeable
  after_validation :set_name
  
  validate :equipment, presence: true
  validate :efficiency, presence: true, numericality: true
  
  private
  def set_name
    if self.name.nil?
      self.name = self.notebook.name
    end
  end
end
