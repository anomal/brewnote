class Style < ActiveRecord::Base
  has_many :notebooks
  
  validates :name, presence: true
  validates :description, presence: true
  validates :abv, presence: true
  
end
