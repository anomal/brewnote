class PrimaryFermentation < ActiveRecord::Base
  belongs_to :fermentation
  has_many :primary_fermentation_days, :dependent => :destroy
  
  validates :volume, presence: true, numericality: true
  validates :blg, presence: true, numericality: true
end
