class StylesController < ApplicationController
  before_action :set_style, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @styles = Style.all
    respond_with(@styles)
  end

  def show
    respond_with(@style)
  end

  def new
    if(user_signed_in?)
      @style = Style.new
      respond_with(@style)
    else
      respond_to do |format|
        format.html { redirect_to new_user_session_path, notice: "You must be logged in to view this content." }
        format.json { head :no_content }
      end
    end
  end

  def edit
  end

  def create
    @style = Style.new(style_params)
    @style.save
    respond_with(@style)
  end

  def update
    @style.update(style_params)
    respond_with(@style)
  end

  def destroy
    @style.destroy
    respond_with(@style)
  end

  private
    def set_style
      @style = Style.find(params[:id])
    end

    def style_params
      params.require(:style).permit(:name, :description, :abv)
    end
end
