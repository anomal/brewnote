class FeedsController < ApplicationController
  
  respond_to :html
  
  def index
    if(user_signed_in?)
      users = Follow.where(:follower_id => current_user.id)
      user_ids = users.map(&:followable_id)
      @feeds = Feed.where(:user_id => user_ids).order("created_at DESC")
      @notebooks = current_user.notebooks.order("created_at DESC")
      notebook_ids = @notebooks.map(&:id)
      @formulas = Formula.where(:notebook_id => notebook_ids).order("created_at DESC").limit(3)
      @notebooks = @notebooks[0..2]
      respond_with(@feeds, @notebooks, @formulas)
    else
      respond_to do |format|
        format.html { redirect_to new_user_session_path, notice: "You must be logged in to view this content." }
        format.json { head :no_content }
      end
    end
  end
end