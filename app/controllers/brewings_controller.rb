class BrewingsController < ApplicationController
  before_action :set_brewing, only: [:show, :edit, :update, :destroy]

  respond_to :html
  
  add_breadcrumb '<i class="fa fa-book"></i> Notebooks'.html_safe, :notebooks_path

  def new
    if(user_signed_in?)
      @notebook = Notebook.find(params[:id])
      add_breadcrumb '<i class="fa fa-eye"></i> '.html_safe + @notebook.name, @notebook
      add_breadcrumb '<i class="fa fa-plus"></i> New brewing'.html_safe, nil
      @brewing = Brewing.new(notebook_id: params[:id] )
      respond_with(@brewing)
    else
      respond_to do |format|
        format.html { redirect_to new_user_session_path, notice: "You must be logged in to view this content." }
        format.json { head :no_content }
      end
    end
  end

  def edit
    add_breadcrumb '<i class="fa fa-eye"></i> '.html_safe + @brewing.notebook.name, @brewing.notebook
    add_breadcrumb '<i class="fa fa-edit"></i> Edit brewing'.html_safe, nil
  end

  def create
    @brewing = Brewing.new(brewing_params)
    if @brewing.save
      @notebook = Notebook.find(brewing_params[:notebook_id])
      @notebook.update(:brewing_id => @brewing.id, :progress => 4)
      respond_to do |format|
        format.html { redirect_to @notebook, notice: "Mash in successfully created." }
        format.json { head :no_content }
      end
    else
      render :new
    end
  end

  def update
    if @brewing.update(brewing_params)
      @notebook = @brewing.notebook
      if @notebook.progress == 7 && !@notebook.formula.nil?
        Feed.create(:user_id => current_user.id, :reference_id => @notebook.formula.id, :reference_type => 'formula', :action_desc => 'updated formula')
      end
      respond_to do |format|
        format.html { redirect_to @notebook, notice: "Mash in successfully updated." }
        format.json { head :no_content }
      end
    else
      render :edit
    end
  end

  def destroy
    @notebook = @brewing.notebook
    @brewing.destroy
    @notebook.fermentation.destroy unless @notebook.fermentation.nil?
    @notebook.update(:progress => 3)
    respond_with(@notebook)
  end

  private
    def set_brewing
      @brewing = Brewing.find(params[:id])
    end

    def brewing_params
      params.require(:brewing).permit(:notebook_id, :duration_hours, :duration_minutes, :desc)
    end
end
