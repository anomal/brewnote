class FiltrationsController < ApplicationController
  before_action :set_filtration, only: [:show, :edit, :update, :destroy]

  respond_to :html
  
  add_breadcrumb '<i class="fa fa-book"></i> Notebooks'.html_safe, :notebooks_path

  def new
    if(user_signed_in?)
      @notebook = Notebook.find(params[:id])
      add_breadcrumb '<i class="fa fa-eye"></i> '.html_safe + @notebook.name, @notebook
      add_breadcrumb '<i class="fa fa-plus"></i> New filtration'.html_safe, nil
      @filtration = Filtration.new(notebook_id: params[:id] )
      respond_with(@filtration)
    else
      respond_to do |format|
        format.html { redirect_to new_user_session_path, notice: "You must be logged in to view this content." }
        format.json { head :no_content }
      end
    end
  end

  def edit
    add_breadcrumb '<i class="fa fa-eye"></i> '.html_safe + @filtration.notebook.name, @filtration.notebook
    add_breadcrumb '<i class="fa fa-edit"></i> Edit filtration'.html_safe, nil
  end

  def create 
    @filtration = Filtration.new(filtration_params)
    if @filtration.save
      @notebook = Notebook.find(filtration_params[:notebook_id])
      @notebook.update(:filtration_id => @filtration.id, :progress => 2)
      #respond_with(@notebook)
      respond_to do |format|
        format.html { redirect_to @notebook, notice: "Filtration successfully created." }
        format.json { head :no_content }
      end
    else
      render :new
    end
  end

  def update
    if @filtration.update(filtration_params)
      @notebook = @filtration.notebook
      if @notebook.progress == 7 && !@notebook.formula.nil?
        Feed.create(:user_id => current_user.id, :reference_id => @notebook.formula.id, :reference_type => 'formula', :action_desc => 'updated formula')
      end
      respond_to do |format|
        format.html { redirect_to @notebook, notice: "Mash in successfully updated." }
        format.json { head :no_content }
      end
    else
      render :edit
    end
  end

  def destroy
    @notebook = @filtration.notebook
    @filtration.destroy
    @notebook.sweetening.destroy unless @notebook.sweetening.nil?
    @notebook.brewing.destroy unless @notebook.brewing.nil?
    @notebook.fermentation.destroy unless @notebook.fermentation.nil?
    @notebook.update(:progress => 1)
    respond_with(@notebook)
  end

  private
    def set_filtration
      @filtration = Filtration.find(params[:id])
    end

    def filtration_params
      params.require(:filtration).permit(:notebook_id, :wort, :blg, :duration_hours, :duration_minutes, :desc)
    end
end
