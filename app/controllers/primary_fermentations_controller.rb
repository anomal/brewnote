class PrimaryFermentationsController < ApplicationController
  before_action :set_primary_fermentation, only: [:show, :edit, :update, :destroy]

  respond_to :html

  add_breadcrumb '<i class="fa fa-book"></i> Notebooks'.html_safe, :notebooks_path
  
  def new
    if(user_signed_in?)
      @fermentation = Fermentation.find(params[:id])
      add_breadcrumb '<i class="fa fa-eye"></i> '.html_safe + @fermentation.notebook.name, @fermentation.notebook
      add_breadcrumb @fermentation.notebook.name + '\'s fermentation', nil
      add_breadcrumb '<i class="fa fa-plus"></i> New primary fermentation'.html_safe, nil
      @primary_fermentation = PrimaryFermentation.new(fermentation_id: params[:id])
      respond_with(@primary_fermentation)
    else
      respond_to do |format|
        format.html { redirect_to new_user_session_path, notice: "You must be logged in to view this content." }
        format.json { head :no_content }
      end
    end
  end

  def edit
    add_breadcrumb '<i class="fa fa-eye"></i> '.html_safe + @primary_fermentation.fermentation.notebook.name, @primary_fermentation.fermentation.notebook
    add_breadcrumb @primary_fermentation.fermentation.notebook.name + '\'s fermentation', nil
    add_breadcrumb '<i class="fa fa-edit"></i> Edit primary fermentation'.html_safe, nil
  end

  def create
    @primary_fermentation = PrimaryFermentation.new(primary_fermentation_params)
    if @primary_fermentation.save
      @fermentation = Fermentation.find(primary_fermentation_params[:fermentation_id])
      @fermentation.update(:primary_fermentation_id => @primary_fermentation.id)
      @notebook = @primary_fermentation.fermentation.notebook
      @notebook.update(:progress => 6)
      respond_to do |format|
        format.html { redirect_to @notebook, notice: "Primary fermentation successfully created." }
        format.json { head :no_content }
      end
    else
      render :new
    end
  end

  def update
    if @primary_fermentation.update(primary_fermentation_params)
      @notebook = @primary_fermentation.fermentation.notebook
      if @notebook.progress == 8 && !@notebook.formula.nil?
        Feed.create(:user_id => current_user.id, :reference_id => @notebook.formula.id, :reference_type => 'formula', :action_desc => 'updated formula')
      end
      respond_to do |format|
        format.html { redirect_to @notebook, notice: "Primary fermentation successfully updated." }
        format.json { head :no_content }
      end
    else
      render :edit
    end
  end

  def destroy
    @notebook = @primary_fermentation.fermentation.notebook
    @primary_fermentation.destroy
    @notebook.fermentation.secondary_fermentation.destroy unless @notebook.fermentation.secondary_fermentation.nil?
    @notebook.update(:progress => 5)
    respond_to do |format|
      format.html { redirect_to @notebook, notice: "Primary fermentation successfully deleted." }
      format.json { head :no_content }
    end
  end

  private
    def set_primary_fermentation
      @primary_fermentation = PrimaryFermentation.find(params[:id])
    end

    def primary_fermentation_params
      params.require(:primary_fermentation).permit(:blg, :volume, :fermentation_id)
    end
end
