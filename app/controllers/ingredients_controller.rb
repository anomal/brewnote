class IngredientsController < ApplicationController
  before_action :set_ingredient, only: [:show, :edit, :update, :destroy]

  respond_to :html
  $types = [
      "malt",
      "hop",
      "yeast"
  ]
  def new
    if(user_signed_in?)
      @ingredient = Ingredient.new(notebook_id: params[:id] )
      @notebook = Notebook.find(params[:id])
      if @notebook.progress == 8 && !@notebook.formula.nil?
        Feed.create(:user_id => current_user.id, :reference_id => @notebook.formula.id, :reference_type => 'formula', :action_desc => 'updated formula')
      end
      respond_with(@ingredient)
    else
      respond_to do |format|
        format.html { redirect_to new_user_session_path, notice: "You must be logged in to view this content." }
        format.json { head :no_content }
      end
    end
  end

  def edit
  end

  def create
    @ingredient = Ingredient.new(ingredient_params)
    #@ingredient.save
    @notebook = Notebook.find(ingredient_params[:notebook_id])
    if @ingredient.save
      respond_with do |format|
        format.html { redirect_to @notebook, notice: "Ingredient successfully added." }
      end
    else
      render :new
    end
  end

  def update
    if @ingredient.update(ingredient_params)
      @notebook = @ingredient.notebook
      if @notebook.progress == 8 && !@notebook.formula.nil?
        Feed.create(:user_id => current_user.id, :reference_id => @notebook.formula.id, :reference_type => 'formula', :action_desc => 'updated formula')
      end
      respond_to do |format|
        format.html { redirect_to @notebook, notice: "Ingredient successfully updated." }
      end
    else
      render :edit
    end
  end

  def destroy
    @notebook = @ingredient.notebook
    @ingredient.destroy
    respond_to do |format|
      format.html { redirect_to @notebook, notice: "Ingredient successfully deleted." }
      format.json { head :no_content }
    end
  end

  private
    def set_ingredient
      @ingredient = Ingredient.find(params[:id])
    end

    def ingredient_params
      params.require(:ingredient).permit(:notebook_id, :kind, :name, :quantity)
    end
end
