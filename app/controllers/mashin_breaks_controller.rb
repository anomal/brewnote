class MashinBreaksController < ApplicationController
  before_action :set_mashin_break, only: [:show, :edit, :update, :destroy]

  respond_to :html
  
  add_breadcrumb '<i class="fa fa-book"></i> Notebooks'.html_safe, :notebooks_path
  
  $types = [
      "heat",
      "break"
  ]

  def new
    #@mashin_break = MashinBreak.new
    if(user_signed_in?)
      @mashin = Mashin.find(params[:id])
      add_breadcrumb '<i class="fa fa-eye"></i> '.html_safe + @mashin.notebook.name, @mashin.notebook, :options => { :title => "Notebook" }
      add_breadcrumb @mashin.notebook.name + '\'s mash', nil, :options => { :title => "Notebook" }
      add_breadcrumb '<i class="fa fa-plus"></i> New mash break'.html_safe, nil
      @mashin_break = MashinBreak.new(mashin_id: params[:id] )
      respond_with(@mashin_break)
    else
      respond_to do |format|
        format.html { redirect_to new_user_session_path, notice: "You must be logged in to view this content." }
        format.json { head :no_content }
      end
    end
  end

  def edit
    add_breadcrumb '<i class="fa fa-eye"></i> '.html_safe + @mashin_break.mashin.notebook.name, @mashin_break.mashin.notebook, :options => { :title => "Notebook" }
    add_breadcrumb @mashin_break.mashin.notebook.name + '\'s mash', nil, :options => { :title => "Notebook" }
    add_breadcrumb '<i class="fa fa-edit"></i> Edit mash break'.html_safe, nil
  end

  def create
    @mashin_break = MashinBreak.new(mashin_break_params)
    if @mashin_break.save
      @notebook = @mashin_break.mashin.notebook
      if @notebook.progress == 7 && !@notebook.formula.nil?
        Feed.create(:user_id => current_user.id, :reference_id => @notebook.formula.id, :reference_type => 'formula', :action_desc => 'updated formula')
      end
      respond_to do |format|
        format.html { redirect_to @notebook, notice: "Mash in break successfully created." }
        format.json { head :no_content }
      end
    else
      render :new
    end
  end

  def update
    if @mashin_break.update(mashin_break_params)
      @notebook = @mashin_break.mashin.notebook
      if @notebook.progress == 7 && !@notebook.formula.nil?
        Feed.create(:user_id => current_user.id, :reference_id => @notebook.formula.id, :reference_type => 'formula', :action_desc => 'updated formula')
      end
      respond_to do |format|
        format.html { redirect_to @notebook, notice: "Mash in successfully updated." }
        format.json { head :no_content }
      end
    else
      render :edit
    end
  end

  def destroy
    @notebook = @mashin_break.mashin.notebook
    @mashin_break.destroy
    if @notebook.progress == 7 && !@notebook.formula.nil?
      Feed.create(:user_id => current_user.id, :reference_id => @notebook.formula.id, :reference_type => 'formula', :action_desc => 'updated formula')
    end
    respond_to do |format|
      format.html { redirect_to @notebook, notice: "Mash in break successfully deleted." }
      format.json { head :no_content }
    end
  end

  private
    def set_mashin_break
      @mashin_break = MashinBreak.find(params[:id])
    end

    def mashin_break_params
      params.require(:mashin_break).permit(:mashin_id, :entry_temp, :result_temp, :duration_hours, :duration_minutes, :step_type)
    end
end
