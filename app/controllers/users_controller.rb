class UsersController < ApplicationController
  def index
    add_breadcrumb '<i class="fa fa-users"></i> People'.html_safe, nil
    @users = User.all
  end

  def show
    @user = User.find(params[:id])
    @notebooks = @user.notebooks
    notebook_ids = @notebooks.map(&:id)
    @formulas = Formula.where(:notebook_id => notebook_ids)
    @following = Follow.where(:follower_id => params[:id]).count
    @followers = Follow.where(:followable_id => params[:id]).count
    @likes = Like.where(:liker_id => params[:id]).count
    
    add_breadcrumb '<i class="fa fa-users"></i> People'.html_safe, users_index_path
    add_breadcrumb '<i class="fa fa-user"></i> '.html_safe + @user.full_name, nil
  end
  
  def follow
    user = current_user
    user_to_follow = User.find(params[:id])
    user.follow!(user_to_follow)
    feed = Feed.new(:user_id => current_user.id, :reference_id => params[:id], :reference_type => 'user', :action_desc => 'followed user')
    feed.save
    respond_to do |format|
      format.html { redirect_to users_index_path, notice: "You are now following #{user_to_follow.name}." }
      format.json { head :no_content }
    end
  end
  
  def unfollow
    user = current_user
    user_to_unfollow = User.find(params[:id])
    user.unfollow!(user_to_unfollow)
    respond_to do |format|
      format.html { redirect_to users_index_path, notice: "You stopped following #{user_to_unfollow.name}." }
      format.json { head :no_content }
    end
  end
  
  private

    def user_params
      params.require(:user).permit(:name, :surname, :avatar, :desc, :birth_date)
    end
end
