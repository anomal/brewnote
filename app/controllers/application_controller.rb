class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_filter :require_login, unless: :devise_controller?
  
  add_breadcrumb '<i class="fa fa-home"></i> Home'.html_safe, :root_path, :options => { :title => "Home" }
  
  
  private

    def require_login
      unless current_user
        redirect_to new_user_session_path
      end
    end
   
   protected

   def configure_permitted_parameters
     #devise_parameter_sanitizer.for(:sign_up) << :name << :surname << :birth_date << :desc << :avatar
     devise_parameter_sanitizer.for(:sign_up) {|u| u.permit(:password, :email, :name, :surname, :birth_date, :desc, :avatar)}
     devise_parameter_sanitizer.for(:account_update) {|u| u.permit(:name, :surname, :birth_date, :desc, :avatar, :current_password)}
   end

end
