class BottlingsController < ApplicationController
  before_action :set_bottling, only: [:show, :edit, :update, :destroy]

  respond_to :html
  
  add_breadcrumb '<i class="fa fa-book"></i> Notebooks'.html_safe, :notebooks_path
  
  $materials = [
      "saccharose",
      "glucose",
      "malt_extract"
  ]
  def new 
    if(user_signed_in?)
      @fermentation = Fermentation.find(params[:id])
      add_breadcrumb '<i class="fa fa-eye"></i> '.html_safe + @fermentation.notebook.name, @fermentation.notebook
      add_breadcrumb @fermentation.notebook.name + '\'s fermentation', nil
      add_breadcrumb '<i class="fa fa-plus"></i> New bottling'.html_safe, nil
      @bottling = Bottling.new(fermentation_id: params[:id])
      respond_with(@bottling)
    else
      respond_to do |format|
        format.html { redirect_to new_user_session_path, notice: "You must be logged in to view this content." }
        format.json { head :no_content }
      end
    end
  end

  def edit
    add_breadcrumb '<i class="fa fa-eye"></i> '.html_safe + @bottling.fermentation.notebook.name, @bottling.fermentation.notebook
    add_breadcrumb @bottling.fermentation.notebook.name + '\'s fermentation', nil
    add_breadcrumb '<i class="fa fa-edit"></i> Edit bottling'.html_safe, nil
  end

  def create
    @bottling = Bottling.new(bottling_params)
    if @bottling.save
      @fermentation = Fermentation.find(bottling_params[:fermentation_id])
      @fermentation.update(:bottling_id => @bottling.id)
      @notebook = @fermentation.notebook
      @notebook.update(:progress => 7)
      if @bottling.blg && @fermentation.blg
        abv = (@fermentation.blg - @bottling.blg)/1.938
        @notebook.update(:abv => abv)
      end
    
      respond_to do |format|
        format.html { redirect_to @notebook, notice: "Bottling successfully created." }
        format.json { head :no_content }
      end
    else
      render :new
    end
  end

  def update
    if @bottling.update(bottling_params)
      @notebook = @bottling.fermentation.notebook
      if @notebook.progress == 7 && !@notebook.formula.nil?
        Feed.create(:user_id => current_user.id, :reference_id => @notebook.formula.id, :reference_type => 'formula', :action_desc => 'updated formula')
      end
      respond_to do |format|
        format.html { redirect_to @notebook, notice: "Bottling successfully updated." }
        format.json { head :no_content }
      end
    else
      render :edit
    end
  end

  def destroy
    @notebook = @bottling.fermentation.notebook
    @notebook.update(:progress => 6)
    @bottling.destroy
    respond_to do |format|
      format.html { redirect_to @notebook, notice: "Primary fermentation successfully deleted." }
      format.json { head :no_content }
    end
  end

  private
    def set_bottling
      @bottling = Bottling.find(params[:id])
    end

    def bottling_params
      params.require(:bottling).permit(:fermentation_id, :blg, :volume, :temperature, :refermentation_material, :refermentation_material_quantity, :material_per_liter)
    end
end
