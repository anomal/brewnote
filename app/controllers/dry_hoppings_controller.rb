class DryHoppingsController < ApplicationController
  before_action :set_dry_hopping, only: [:show, :edit, :update, :destroy]

  respond_to :html
  
  add_breadcrumb '<i class="fa fa-book"></i> Notebooks'.html_safe, :notebooks_path

  def new
    if(user_signed_in?)
      @secondary_fermentation = SecondaryFermentation.find(params[:id])
      add_breadcrumb '<i class="fa fa-eye"></i> '.html_safe + @secondary_fermentation.fermentation.notebook.name, @secondary_fermentation.fermentation.notebook
      add_breadcrumb @secondary_fermentation.fermentation.notebook.name + '\'s fermentation', nil
      add_breadcrumb '<i class="fa fa-plus"></i> New dry hopping'.html_safe, nil
      @dry_hopping = DryHopping.new(secondary_fermentation_id: params[:id] )
      @notebook = @dry_hopping.secondary_fermentation.fermentation.notebook
      respond_with(@notebook)
    else
      respond_to do |format|
        format.html { redirect_to new_user_session_path, notice: "You must be logged in to view this content." }
        format.json { head :no_content }
      end
    end
  end

  def edit
    add_breadcrumb '<i class="fa fa-eye"></i> '.html_safe + @dry_hopping.secondary_fermentation.fermentation.notebook.name, @dry_hopping.secondary_fermentation.fermentation.notebook
    add_breadcrumb @dry_hopping.secondary_fermentation.fermentation.notebook.name + '\'s fermentation', nil
    add_breadcrumb '<i class="fa fa-edit"></i> Edit dry hopping'.html_safe, nil
  end

  def create
    @dry_hopping = DryHopping.new(dry_hopping_params)
    if @dry_hopping.save
      @notebook = @dry_hopping.secondary_fermentation.fermentation.notebook
      if @notebook.progress == 8 && !@notebook.formula.nil?
        Feed.create(:user_id => current_user.id, :reference_id => @notebook.formula.id, :reference_type => 'formula', :action_desc => 'updated formula')
      end
      respond_to do |format|
        format.html { redirect_to @notebook, notice: "Hopping successfully created." }
        format.json { head :no_content }
      end
    else
      render :new
    end
  end

  def update
    if @dry_hopping.update(dry_hopping_params)
      @notebook = @dry_hopping.secondary_fermentation.fermentation.notebook
      if @notebook.progress == 8 && !@notebook.formula.nil?
        Feed.create(:user_id => current_user.id, :reference_id => @notebook.formula.id, :reference_type => 'formula', :action_desc => 'updated formula')
      end
      respond_to do |format|
        format.html { redirect_to @notebook, notice: "Hopping successfully updated." }
        format.json { head :no_content }
      end
    else
      render :edit
    end
  end

  def destroy
    @notebook = @dry_hopping.secondary_fermentation.fermentation.notebook
    @dry_hopping.destroy
    if @notebook.progress == 8 && !@notebook.formula.nil?
      Feed.create(:user_id => current_user.id, :reference_id => @notebook.formula.id, :reference_type => 'formula', :action_desc => 'updated formula')
    end
    respond_to do |format|
      format.html { redirect_to @notebook, notice: "Hopping successfully deleted." }
      format.json { head :no_content }
    end
  end

  private
    def set_dry_hopping
      @dry_hopping = DryHopping.find(params[:id])
    end

    def dry_hopping_params
      params.require(:dry_hopping).permit(:secondary_fermentation_id, :name, :quantity, :days_before_end)
    end
end
