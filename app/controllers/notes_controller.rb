class NotesController < ApplicationController
  before_action :set_note, only: [:show, :edit, :update, :destroy]

  respond_to :html
  
  add_breadcrumb '<i class="fa fa-book"></i> Notebooks'.html_safe, :notebooks_path
  
  def show
    add_breadcrumb '<i class="fa fa-eye"></i> '.html_safe + @note.notebook.name, @note.notebook
    add_breadcrumb  @note.name, nil
    respond_with(@note)
  end

  def new
    if(user_signed_in?)
      @notebook = Notebook.find(params[:id])
      add_breadcrumb '<i class="fa fa-eye"></i> '.html_safe + @notebook.name, @notebook
      add_breadcrumb '<i class="fa fa-plus"></i> New note'.html_safe, nil
      @note = Note.new(notebook_id: params[:id] )
      respond_with(@note)
    else
      respond_to do |format|
        format.html { redirect_to new_user_session_path, notice: "You must be logged in to view this content." }
        format.json { head :no_content }
      end
    end
  end

  def edit
    add_breadcrumb '<i class="fa fa-eye"></i> '.html_safe + @note.notebook.name, @note.notebook
    add_breadcrumb '<i class="fa fa-edit"></i> Edit note'.html_safe, nil
  end

  def create
    @note = Note.new(note_params)
    if @note.save
      @notebook = Notebook.find(note_params[:notebook_id])
      respond_to do |format|
        format.html { redirect_to @notebook, notice: "Note successfully created." }
        format.json { head :no_content }
      end
    else
      render :new
    end
  end

  def update
    if @note.update(note_params)
      @notebook = @note.notebook
      respond_to do |format|
        format.html { redirect_to @notebook, notice: "Note successfully updated." }
        format.json { head :no_content }
      end
    else
      render :edit
    end
  end

  def destroy
    @notebook = @note.notebook
    @note.destroy
    respond_with(@notebook)
  end

  private
    def set_note
      @note = Note.find(params[:id])
    end

    def note_params
      params.require(:note).permit(:name, :description, :date, :notebook_id)
    end
end
