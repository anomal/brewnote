class NotebooksController < ApplicationController
  before_action :set_notebook, only: [:show, :edit, :update, :destroy]

  respond_to :html
  
  add_breadcrumb '<i class="fa fa-book"></i> Notebooks'.html_safe, :notebooks_path, :options => { :title => "Notebooks" }
  
  def index
    if(user_signed_in?)
      @notebooks = current_user.notebooks
      respond_with(@notebooks)
    else
      respond_to do |format|
        format.html { redirect_to new_user_session_path, notice: "You must be logged in to view this content." }
        format.json { head :no_content }
      end
    end
  end

  def show
    if(current_user.id == @notebook.user_id)
      add_breadcrumb '<i class="fa fa-eye"></i> '.html_safe + @notebook.name, :notebook_path, :options => { :title => "Notebook" }
      @mashin = @notebook.mashin
      @filtration = @notebook.filtration
      @sweetening = @notebook.sweetening
      @brewing = @notebook.brewing
      @fermentation = @notebook.fermentation
      respond_with(@notebook, @mashin, @filtration, @sweetening, @brewing, @fermentation)
    else
      respond_to do |format|
        format.html { redirect_to notebooks_path, notice: "It seems you are not an owner of this notebook!" }
        format.json { head :no_content }
      end
    end
  end

  def new
    add_breadcrumb '<i class="fa fa-plus"></i> New notebook'.html_safe, nil
    if(user_signed_in?)
      @notebook = Notebook.new
      @styles = Style.all
      respond_with(@notebook, @styles)
    else
      respond_to do |format|
        format.html { redirect_to new_user_session_path, notice: "You must be logged in to view this content." }
        format.json { head :no_content }
      end
    end
  end

  def edit
    add_breadcrumb '<i class="fa fa-eye"></i> '.html_safe + @notebook.name, :notebook_path, :options => { :title => "Notebooks" }
    add_breadcrumb '<i class="fa fa-edit"></i> Edit '.html_safe + @notebook.name, :edit_notebook_path, :options => { :title => "Edit notebook" }
    if(!user_signed_in?)
      respond_to do |format|
        format.html { redirect_to new_user_session_path, notice: "You must be logged in to view this content." }
        format.json { head :no_content }
      end
    else
      if(current_user.id == @notebook.user_id)
        respond_with(@notebook)
      else
        respond_to do |format|
          format.html { redirect_to notebooks_path, notice: "It seems you are not an owner of this notebook!" }
          format.json { head :no_content }
        end
      end
    end
  end

  def create
    @notebook = Notebook.new(notebook_params.merge(:user_id => current_user.id))
    if @notebook.save
      respond_to do |format|
        format.html { redirect_to @notebook, notice: "Notebook sucessfully created." }
      end
    else
      render :new
    end
  end

  def update
    if @notebook.update(notebook_params)
      if @notebook.progress == 8 && !@notebook.formula.nil?
        Feed.create(:user_id => current_user.id, :reference_id => @notebook.formula.id, :reference_type => 'formula', :action_desc => 'updated formula')
      end
      respond_to do |format|
        format.html { redirect_to @notebook, notice: "Notebook sucessfully updated." }
      end
    else
      render :edit
    end
  end

  def destroy
    if(user_signed_in?)
      if(current_user.id == @notebook.user_id)
        @notebook.destroy
        respond_with(@notebook)
      else
        respond_to do |format|
          format.html { redirect_to notebooks_path, notice: "You must be owner of this Notebook to delete it." }
          format.json { head :no_content }
        end
      end
    else
      respond_to do |format|
        format.html { redirect_to new_user_session_path, notice: "You must be logged in to view this content." }
        format.json { head :no_content }
      end
    end
  end

  private
    def set_notebook
      @notebook = Notebook.find(params[:id])
    end

    def notebook_params
      params.require(:notebook).permit(:user_id, :bjcp_id, :description, :name, :malts, :hops, :yeast, :abv, :ibu_min, :ibu_max, :number, :mashin_id, :filtration_id, :sweetening_id, :brewing_id, :fermentation_id, :progress)
    end
end
