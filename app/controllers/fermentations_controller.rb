class FermentationsController < ApplicationController
  before_action :set_fermentation, only: [:show, :edit, :update, :destroy]

  respond_to :html
  add_breadcrumb '<i class="fa fa-book"></i> Notebooks'.html_safe, :notebooks_path
  
  $forms = [
      "rehydratation",
      "liquid_yeast",
      "slurry"
  ]
  def new
    if(user_signed_in?)
      @notebook = Notebook.find(params[:id])
      add_breadcrumb '<i class="fa fa-eye"></i> '.html_safe + @notebook.name, @notebook
      add_breadcrumb '<i class="fa fa-plus"></i> New fermentation'.html_safe, nil
      @fermentation = Fermentation.new(notebook_id: params[:id] )
      respond_with(@fermentation, $forms)
    else
      respond_to do |format|
        format.html { redirect_to new_user_session_path, notice: "You must be logged in to view this content." }
        format.json { head :no_content }
      end
    end
  end

  def edit
    add_breadcrumb '<i class="fa fa-eye"></i> '.html_safe + @fermentation.notebook.name, @fermentation.notebook
    add_breadcrumb '<i class="fa fa-edit"></i> Edit fermentation'.html_safe, nil
  end

  def create
    @fermentation = Fermentation.new(fermentation_params)
    if @fermentation.save
      @notebook = Notebook.find(fermentation_params[:notebook_id])
      @notebook.update(:fermentation_id => @fermentation.id, :progress => 5)
      respond_to do |format|
        format.html { redirect_to @notebook, notice: "Fermentation successfully created." }
        format.json { head :no_content }
      end
    else
      render :new
    end
  end

  def update
    if @fermentation.update(fermentation_params)
      @notebook = @fermentation.notebook
      if @notebook.progress == 7 && !@notebook.formula.nil?
        Feed.create(:user_id => current_user.id, :reference_id => @notebook.formula.id, :reference_type => 'formula', :action_desc => 'updated formula')
      end
      respond_to do |format|
        format.html { redirect_to @notebook, notice: "Fermentation successfully updated." }
        format.json { head :no_content }
      end
    else
      render :edit
    end
  end

  def destroy
    @notebook = @fermentation.notebook
    @fermentation.destroy
    @notebook.update(:progress => 4)
    respond_with(@notebook)
  end

  private
    def set_fermentation
      @fermentation = Fermentation.find(params[:id])
    end

    def fermentation_params
      params.require(:fermentation).permit(:notebook_id, :quantity, :blg, :start_temperature, :yeast_form, :yeast_quantity, :desc, :primary_fermentation_id, :secondary_fermentation_id, :bottling_id)
    end
end
