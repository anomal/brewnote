class BrewingHopsController < ApplicationController
  before_action :set_brewing_hop, only: [:show, :edit, :update, :destroy]

  respond_to :html
  
  add_breadcrumb '<i class="fa fa-book"></i> Notebooks'.html_safe, :notebooks_path
  
  def new
    if(user_signed_in?)
      @brewing = Brewing.find(params[:id])
      add_breadcrumb '<i class="fa fa-eye"></i> '.html_safe + @brewing.notebook.name, @brewing.notebook
      add_breadcrumb @brewing.notebook.name + '\'s brewing', nil
      add_breadcrumb '<i class="fa fa-plus"></i> New brewing hop'.html_safe, nil
      @brewing_hop = BrewingHop.new(brewing_id: params[:id] )
      @notebook = @brewing_hop.brewing.notebook
      respond_with(@notebook)
    else
      respond_to do |format|
        format.html { redirect_to new_user_session_path, notice: "You must be logged in to view this content." }
        format.json { head :no_content }
      end
    end
  end

  def edit
    add_breadcrumb '<i class="fa fa-eye"></i> '.html_safe + @brewing_hop.brewing.notebook.name, @brewing_hop.brewing.notebook
    add_breadcrumb @brewing_hop.brewing.notebook.name + '\'s brewing', nil
    add_breadcrumb '<i class="fa fa-edit"></i> Edit brewing hop'.html_safe, nil
  end

  def create
    @brewing_hop = BrewingHop.new(brewing_hop_params)
    if @brewing_hop.save
      @notebook = @brewing_hop.brewing.notebook
      if @notebook.progress == 7 && !@notebook.formula.nil?
        Feed.create(:user_id => current_user.id, :reference_id => @notebook.formula.id, :reference_type => 'formula', :action_desc => 'updated formula')
      end
      respond_to do |format|
        format.html { redirect_to @notebook, notice: "Hopping successfully created." }
        format.json { head :no_content }
      end
    else 
      render :new
    end
  end

  def update
    if @brewing_hop.update(brewing_hop_params)
      @notebook = @brewing_hop.brewing.notebook
      if @notebook.progress == 7 && !@notebook.formula.nil?
        Feed.create(:user_id => current_user.id, :reference_id => @notebook.formula.id, :reference_type => 'formula', :action_desc => 'updated formula')
      end
      respond_to do |format|
        format.html { redirect_to @notebook, notice: "Hopping successfully updated." }
        format.json { head :no_content }
      end
    else
      render :edit
    end
  end

  def destroy
    @notebook = @brewing_hop.brewing.notebook
    @brewing_hop.destroy
    if @notebook.progress == 7 && !@notebook.formula.nil?
      Feed.create(:user_id => current_user.id, :reference_id => @notebook.formula.id, :reference_type => 'formula', :action_desc => 'updated formula')
    end
    respond_to do |format|
      format.html { redirect_to @notebook, notice: "Hopping successfully deleted." }
      format.json { head :no_content }
    end
  end

  private
    def set_brewing_hop
      @brewing_hop = BrewingHop.find(params[:id])
    end

    def brewing_hop_params
      params.require(:brewing_hop).permit(:brewing_id, :hop_name, :hop_quantity, :minutes_before_end)
    end
end
