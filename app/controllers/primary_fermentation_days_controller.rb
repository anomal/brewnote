class PrimaryFermentationDaysController < ApplicationController
  before_action :set_primary_fermentation_day, only: [:show, :edit, :update, :destroy]

  respond_to :html
  
  add_breadcrumb '<i class="fa fa-book"></i> Notebooks'.html_safe, :notebooks_path
  
  def new
    if(user_signed_in?)
      @fermentation = Fermentation.find(params[:id])
      add_breadcrumb '<i class="fa fa-eye"></i> '.html_safe + @fermentation.notebook.name, @fermentation.notebook
      add_breadcrumb @fermentation.notebook.name + '\'s fermentation', nil
      add_breadcrumb '<i class="fa fa-plus"></i> New primary fermentation day'.html_safe, nil
      @primary_fermentation_day = PrimaryFermentationDay.new(fermentation_id: params[:id])
      respond_with(@primary_fermentation_day)
    else
      respond_to do |format|
        format.html { redirect_to new_user_session_path, notice: "You must be logged in to view this content." }
        format.json { head :no_content }
      end
    end
  end

  def edit
    add_breadcrumb '<i class="fa fa-eye"></i> '.html_safe + @primary_fermentation_day.fermentation.notebook.name, @primary_fermentation_day.fermentation.notebook
    add_breadcrumb @primary_fermentation_day.fermentation.notebook.name + '\'s fermentation', nil
    add_breadcrumb '<i class="fa fa-edit"></i> Edit primary fermentation day'.html_safe, nil
  end

  def create
    @primary_fermentation_day = PrimaryFermentationDay.new(primary_fermentation_day_params)
    if @primary_fermentation_day.save
      @notebook = @primary_fermentation_day.fermentation.notebook
      if @notebook.progress == 7 && !@notebook.formula.nil?
        Feed.create(:user_id => current_user.id, :reference_id => @notebook.formula.id, :reference_type => 'formula', :action_desc => 'updated formula')
      end
      respond_to do |format|
        format.html { redirect_to @notebook, notice: "Day successfully created." }
        format.json { head :no_content }
      end
    else
      render :new
    end
  end

  def update
    if @primary_fermentation_day.update(primary_fermentation_day_params)
      @notebook = @primary_fermentation_day.fermentation.notebook
      if @notebook.progress == 7 && !@notebook.formula.nil?
        Feed.create(:user_id => current_user.id, :reference_id => @notebook.formula.id, :reference_type => 'formula', :action_desc => 'updated formula')
      end
      respond_to do |format|
        format.html { redirect_to @notebook, notice: "Day successfully updated." }
        format.json { head :no_content }
      end
    else
      render :edit
    end
  end

  def destroy
    @notebook = @primary_fermentation_day.fermentation.notebook
    @primary_fermentation_day.destroy
    if @notebook.progress == 7 && !@notebook.formula.nil?
      Feed.create(:user_id => current_user.id, :reference_id => @notebook.formula.id, :reference_type => 'formula', :action_desc => 'updated formula')
    end
    respond_to do |format|
      format.html { redirect_to @notebook, notice: "Primary fermentation successfully deleted." }
      format.json { head :no_content }
    end
  end

  private
    def set_primary_fermentation_day
      @primary_fermentation_day = PrimaryFermentationDay.find(params[:id])
    end

    def primary_fermentation_day_params
      params.require(:primary_fermentation_day).permit(:fermentation_id, :day, :temperature)
    end
end
