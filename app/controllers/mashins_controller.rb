class MashinsController < ApplicationController
  before_action :set_mashin, only: [:show, :edit, :update, :destroy]

  respond_to :html

  add_breadcrumb '<i class="fa fa-book"></i> Notebooks'.html_safe, :notebooks_path, :options => { :title => "Notebooks" }
  
  def new
    if(user_signed_in?)
      @notebook = Notebook.find(params[:id])
      add_breadcrumb '<i class="fa fa-eye"></i> '.html_safe + @notebook.name, @notebook, :options => { :title => "Notebook" }
      add_breadcrumb '<i class="fa fa-plus"></i> New mash'.html_safe, nil, :options => { :title => "New mash" }
      @mashin = Mashin.new(notebook_id: params[:id] )
      respond_with(@mashin)
    else
      respond_to do |format|
        format.html { redirect_to new_user_session_path, notice: "You must be logged in to view this content." }
        format.json { head :no_content }
      end
    end
  end

  def edit
    add_breadcrumb '<i class="fa fa-eye"></i> '.html_safe + @mashin.notebook.name, @mashin.notebook, :options => { :title => "Notebook" }
    add_breadcrumb '<i class="fa fa-edit"></i> Edit mash-in'.html_safe, nil, :options => { :title => "Edit mash-in" }
  end

  def create
    @mashin = Mashin.new(mashin_params)
    if @mashin.save
      @notebook = Notebook.find(mashin_params[:notebook_id])
      @notebook.update(:mashin_id => @mashin.id, :progress => 1)
      respond_to do |format|
        format.html { redirect_to @notebook, notice: "Mash in successfully created." }
      end
    else
      render :new
    end
  end

  def update
    if @mashin.update(mashin_params)
      @notebook = Notebook.find(mashin_params[:notebook_id])
      if @notebook.progress == 7 && !@notebook.formula.nil?
        Feed.create(:user_id => current_user.id, :reference_id => @notebook.formula.id, :reference_type => 'formula', :action_desc => 'updated formula')
      end
      respond_to do |format|
        format.html { redirect_to @notebook, notice: "Mash in successfully updated." }
        format.json { head :no_content }
      end
    else
      render :edit
    end
  end

  def destroy
    @notebook = @mashin.notebook
    @mashin.destroy
    @notebook.filtration.destroy unless @notebook.filtration.nil?
    @notebook.sweetening.destroy unless @notebook.sweetening.nil?
    @notebook.brewing.destroy unless @notebook.brewing.nil?
    @notebook.fermentation.destroy unless @notebook.fermentation.nil?
    @notebook.update(:progress => 0)
    respond_with(@notebook)
  end

  private
    def set_mashin
      @mashin = Mashin.find(params[:id])
    end

    def mashin_params
      params.require(:mashin).permit(:notebook_id, :pot, :entry_temp, :result_temp, :duration_hours, :duration_minutes, :desc, :ph, :acid, :acid_volume)
    end
end
