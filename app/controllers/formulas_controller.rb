class FormulasController < ApplicationController
  before_action :set_formula, only: [:show, :edit, :update, :destroy, :like, :unlike]

  respond_to :html
  

  def index
    add_breadcrumb '<i class="fa fa-list-ol"></i> Formulas'.html_safe, nil
    @formulas = Formula.all
    respond_with(@formulas)
  end

  def show
    add_breadcrumb '<i class="fa fa-list-ol"></i> Formulas'.html_safe, :formulas_path
    add_breadcrumb '<i class="fa fa-eye"></i> '.html_safe + @formula.name, nil
    respond_with(@formula)
  end

  def new
    if(!user_signed_in?)
      respond_to do |format|
        format.html { redirect_to new_user_session_path, notice: "You must be logged in to view this content." }
        format.json { head :no_content }
      end
    else
      @notebook = Notebook.find(params[:id])
      if(current_user.id = @notebook.user_id && @notebook.progress == 8)
        add_breadcrumb '<i class="fa fa-book"></i> Notebooks'.html_safe, :notebooks_path
        add_breadcrumb '<i class="fa fa-eye"></i> '.html_safe + @notebook.name, @notebook
        add_breadcrumb '<i class="fa fa-plus"></i> New formula'.html_safe, nil
        @formula = Formula.new(notebook_id: params[:id])
        respond_with(@formula)
      else
        respond_to do |format|
          format.html { redirect_to formulas_path, notice: "It seems this notebook is not yet finished, or you are not an owner!" }
          format.json { head :no_content }
        end
      end
    end
  end

  def edit
    if(!user_signed_in?)
      respond_to do |format|
        format.html { redirect_to new_user_session_path, notice: "You must be logged in to view this content." }
        format.json { head :no_content }
      end
    else
      if(current_user.id == @formula.notebook.user_id)
        respond_with(@formula)
      else
        respond_to do |format|
          format.html { redirect_to formulas_path, notice: "It seems you are not an owner of this formula!" }
          format.json { head :no_content }
        end
      end
    end
  end

  def create
    @formula = Formula.new(formula_params)
    if @formula.save
      @notebook = Notebook.find(formula_params[:notebook_id])
      @notebook.update(:formula_id => @formula.id, :published => 1)
      Feed.create(:user_id => current_user.id, :reference_id => @formula.id, :reference_type => 'formula', :action_desc => 'created new formula')
      respond_to do |format|
        format.html { redirect_to @formula, notice: "Formula successfully created!" }
        format.json { head :no_content }
      end
    else
      render :new
    end
  end

  def update
    if @formula.update(formula_params)
      Feed.create(:user_id => current_user.id, :reference_id => @formula.id, :reference_type => 'formula', :action_desc => 'updated formula')
      respond_to do |format|
        format.html { redirect_to @formula, notice: "Formula successfully updated!" }
        format.json { head :no_content }
      end
    else 
      render :edit
    end
  end

  def destroy
    @notebook = @formula.notebook
    @notebook.update(:formula_id => nil, :published => 0)
    @formula.destroy
    respond_to do |format|
      format.html { redirect_to formulas_path, notice: "Formula successfully deleted!" }
      format.json { head :no_content }
    end
  end
  
  def like
    current_user.like!(@formula)
    Feed.create(:user_id => current_user.id, :reference_id => @formula.id, :reference_type => 'formula', :action_desc => 'liked formula')
    redirect_to formulas_path
  end
  
  def unlike
    current_user.unlike!(@formula)
    redirect_to formulas_path
  end

  private
    def set_formula
      @formula = Formula.find(params[:id])
    end

    def formula_params
      params.require(:formula).permit(:notebook_id, :equipment, :efficiency, :name)
    end
end
