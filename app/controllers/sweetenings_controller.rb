class SweeteningsController < ApplicationController
  before_action :set_sweetening, only: [:show, :edit, :update, :destroy]

  respond_to :html
  
  add_breadcrumb '<i class="fa fa-book"></i> Notebooks'.html_safe, :notebooks_path

  def new
    if(user_signed_in?)
      @notebook = Notebook.find(params[:id])
      add_breadcrumb '<i class="fa fa-eye"></i> '.html_safe + @notebook.name, @notebook
      add_breadcrumb '<i class="fa fa-plus"></i> New sparge'.html_safe, nil
      @sweetening = Sweetening.new(notebook_id: params[:id] )
      respond_with(@sweetening)
    else
      respond_to do |format|
        format.html { redirect_to new_user_session_path, notice: "You must be logged in to view this content." }
        format.json { head :no_content }
      end
    end
  end

  def edit
    add_breadcrumb '<i class="fa fa-eye"></i> '.html_safe + @sweetening.notebook.name, @sweetening.notebook
    add_breadcrumb '<i class="fa fa-edit"></i> Edit sparge'.html_safe, nil
  end

  def create
    @sweetening = Sweetening.new(sweetening_params)
    if @sweetening.save
      @notebook = Notebook.find(sweetening_params[:notebook_id])
      @notebook.update(:sweetening_id => @sweetening.id, :progress => 3)
      #respond_with(@notebook)
      respond_to do |format|
        format.html { redirect_to @notebook, notice: "Sweetening successfully created." }
        format.json { head :no_content }
      end
    else
      render :new
    end
  end

  def update
    if @sweetening.update(sweetening_params)
      @notebook = @sweetening.notebook
      if @notebook.progress == 7 && !@notebook.formula.nil?
        Feed.create(:user_id => current_user.id, :reference_id => @notebook.formula.id, :reference_type => 'formula', :action_desc => 'updated formula')
      end
      respond_to do |format|
        format.html { redirect_to @notebook, notice: "Sweetening successfully updated." }
        format.json { head :no_content }
      end
    else
      render :edit
    end
  end

  def destroy
    @notebook = @sweetening.notebook
    @sweetening.destroy
    @notebook.brewing.destroy unless @notebook.brewing.nil?
    @notebook.fermentation.destroy unless @notebook.fermentation.nil?
    @notebook.update(:progress => 2)
    respond_to do |format|
      format.html { redirect_to @notebook, notice: "Sweetening successfully deleted." }
      format.json { head :no_content }
    end
  end

  private
    def set_sweetening
      @sweetening = Sweetening.find(params[:id])
    end

    def sweetening_params
      params.require(:sweetening).permit(:notebook_id, :water, :ph, :acid_name, :acid, :duration_hours, :duration_minutes, :temperature, :desc)
    end
end
