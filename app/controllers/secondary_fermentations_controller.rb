class SecondaryFermentationsController < ApplicationController
  before_action :set_secondary_fermentation, only: [:show, :edit, :update, :destroy]

  respond_to :html
  
  add_breadcrumb '<i class="fa fa-book"></i> Notebooks'.html_safe, :notebooks_path

  def new 
    if(user_signed_in?)
      @fermentation = Fermentation.find(params[:id])
      add_breadcrumb '<i class="fa fa-eye"></i> '.html_safe + @fermentation.notebook.name, @fermentation.notebook
      add_breadcrumb @fermentation.notebook.name + '\'s fermentation', nil
      add_breadcrumb '<i class="fa fa-plus"></i> New secondary fermentation'.html_safe, nil
      @secondary_fermentation = SecondaryFermentation.new(fermentation_id: params[:id])
      respond_with(@secondary_fermentation)
    else
      respond_to do |format|
        format.html { redirect_to new_user_session_path, notice: "You must be logged in to view this content." }
        format.json { head :no_content }
      end
    end
  end

  def edit
    add_breadcrumb '<i class="fa fa-eye"></i> '.html_safe + @secondary_fermentation.fermentation.notebook.name, @secondary_fermentation.fermentation.notebook
    add_breadcrumb @secondary_fermentation.fermentation.notebook.name + '\'s fermentation', nil
    add_breadcrumb '<i class="fa fa-edit"></i> Edit secondary fermentation'.html_safe, nil
  end

  def create
    @secondary_fermentation = SecondaryFermentation.new(secondary_fermentation_params)
    if @secondary_fermentation.save
      @fermentation = Fermentation.find(secondary_fermentation_params[:fermentation_id])
      @fermentation.update(:secondary_fermentation_id => @secondary_fermentation.id)
      @notebook = @secondary_fermentation.fermentation.notebook
      @notebook.update(:progress => 6)
      respond_to do |format|
        format.html { redirect_to @notebook, notice: "Secondary fermentation successfully created." }
        format.json { head :no_content }
      end
    else
      render :new
    end
  end

  def update
    if @secondary_fermentation.update(secondary_fermentation_params)
      @notebook = @secondary_fermentation.fermentation.notebook
      if @notebook.progress == 7 && !@notebook.formula.nil?
        Feed.create(:user_id => current_user.id, :reference_id => @notebook.formula.id, :reference_type => 'formula', :action_desc => 'updated formula')
      end
      respond_to do |format|
        format.html { redirect_to @notebook, notice: "Secondary fermentation successfully updated." }
        format.json { head :no_content }
      end
    else
      render :edit
    end
  end

  def destroy
    @notebook = @secondary_fermentation.fermentation.notebook
    @secondary_fermentation.destroy
    @notebook.update(:progress => 5)
    respond_to do |format|
      format.html { redirect_to @notebook, notice: "Secondary fermentation successfully deleted." }
      format.json { head :no_content }
    end
  end

  private
    def set_secondary_fermentation
      @secondary_fermentation = SecondaryFermentation.find(params[:id])
    end

    def secondary_fermentation_params
      params.require(:secondary_fermentation).permit(:blg, :volume, :days, :temperature, :fermentation_id)
    end
end
